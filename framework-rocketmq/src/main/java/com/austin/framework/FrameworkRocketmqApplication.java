package com.austin.framework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FrameworkRocketmqApplication {

    public static void main(String[] args) {
        SpringApplication.run(FrameworkRocketmqApplication.class, args);
    }

}
