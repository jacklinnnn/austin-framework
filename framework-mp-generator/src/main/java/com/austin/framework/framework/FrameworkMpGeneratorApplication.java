package com.austin.framework.framework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FrameworkMpGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(FrameworkMpGeneratorApplication.class, args);
    }

}
