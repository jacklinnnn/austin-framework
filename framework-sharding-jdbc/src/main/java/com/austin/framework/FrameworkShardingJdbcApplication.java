package com.austin.framework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author austin
 */
@SpringBootApplication
public class FrameworkShardingJdbcApplication {

    public static void main(String[] args) {
        SpringApplication.run(FrameworkShardingJdbcApplication.class, args);
    }

}
