package com.austin.framework.entity;

import lombok.Data;

/**
 * 订单配置
 *
 * @author: austin
 * @since: 2023/2/25 18:51
 */
@Data
public class OrderConfig {

    /**
     * 编号
     */
    private Integer id;
    /**
     * 支付超时时间
     * <p>
     * 单位：分钟
     */
    private Integer payTimeout;
}
