package com.austin.framework.entity;

import lombok.Data;

/**
 * 订单
 *
 * @author: austin
 * @since: 2023/2/25 18:51
 */
@Data
public class Order {

    /**
     * 订单编号
     */
    private Long id;
    /**
     * 用户编号
     */
    private Integer userId;
}
