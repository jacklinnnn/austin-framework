create
database `network-db0`;
create
database `network-db1`;


use
`network-db0`;

SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for order_0
-- ----------------------------
DROP TABLE IF EXISTS `order_0`;
CREATE TABLE `order_0`
(
    `id`      bigint(11) NOT NULL AUTO_INCREMENT COMMENT '订单编号',
    `user_id` int(16) DEFAULT NULL COMMENT '用户编号',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='订单表';

-- ----------------------------
-- Table structure for order_2
-- ----------------------------
DROP TABLE IF EXISTS `order_2`;
CREATE TABLE `order_2`
(
    `id`      bigint(11) NOT NULL AUTO_INCREMENT COMMENT '订单编号',
    `user_id` int(16) DEFAULT NULL COMMENT '用户编号',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='订单表';

-- ----------------------------
-- Table structure for order_4
-- ----------------------------
DROP TABLE IF EXISTS `order_4`;
CREATE TABLE `order_4`
(
    `id`      bigint(11) NOT NULL AUTO_INCREMENT COMMENT '订单编号',
    `user_id` int(16) DEFAULT NULL COMMENT '用户编号',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='订单表';

-- ----------------------------
-- Table structure for order_6
-- ----------------------------
DROP TABLE IF EXISTS `order_6`;
CREATE TABLE `order_6`
(
    `id`      bigint(11) NOT NULL AUTO_INCREMENT COMMENT '订单编号',
    `user_id` int(16) DEFAULT NULL COMMENT '用户编号',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='订单表';

-- ----------------------------
-- Table structure for order_config
-- ----------------------------
DROP TABLE IF EXISTS `order_config`;
CREATE TABLE `order_config`
(
    `id`          int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
    `pay_timeout` int(11) DEFAULT NULL COMMENT '支付超时时间;单位：分钟',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='订单配置表';


use
`network-db1`;

SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for order_1
-- ----------------------------
DROP TABLE IF EXISTS `order_1`;
CREATE TABLE `order_1`
(
    `id`      bigint(11) NOT NULL AUTO_INCREMENT COMMENT '订单编号',
    `user_id` int(16) DEFAULT NULL COMMENT '用户编号',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=400675304294580226 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='订单表';

-- ----------------------------
-- Table structure for order_3
-- ----------------------------
DROP TABLE IF EXISTS `order_3`;
CREATE TABLE `order_3`
(
    `id`      bigint(11) NOT NULL AUTO_INCREMENT COMMENT '订单编号',
    `user_id` int(16) DEFAULT NULL COMMENT '用户编号',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='订单表';

-- ----------------------------
-- Table structure for order_5
-- ----------------------------
DROP TABLE IF EXISTS `order_5`;
CREATE TABLE `order_5`
(
    `id`      bigint(11) NOT NULL AUTO_INCREMENT COMMENT '订单编号',
    `user_id` int(16) DEFAULT NULL COMMENT '用户编号',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='订单表';

-- ----------------------------
-- Table structure for order_7
-- ----------------------------
DROP TABLE IF EXISTS `order_7`;
CREATE TABLE `order_7`
(
    `id`      bigint(11) NOT NULL AUTO_INCREMENT COMMENT '订单编号',
    `user_id` int(16) DEFAULT NULL COMMENT '用户编号',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='订单表';
