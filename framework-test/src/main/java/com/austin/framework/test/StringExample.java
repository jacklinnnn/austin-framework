package com.austin.framework.test;

/**
 * @author: austin
 * @since: 2023/2/22 15:46
 */
public class StringExample {

    public static final String message = "csdn";

    public static void main(String[] args) {
        // 对于 String a = "cs" + "dn"; 编译器会给你优化成 String a = "csdn";
        String a = "cs" + "dn";

        // 在堆中创建字符串对象“cs”，将字符串对象“cs”的引用保存在字符串常量池中
        String b = "cs";

        // 在堆中创建字符串对象“dn”，将字符串对象“dn”的引用保存在字符串常量池中
        String c = "dn";

        String d = b + c;

        // false
        System.out.println(a == d);

        // true
        System.out.println(a == message);

        // false
        System.out.println((b + c) == message);
    }
}
