package com.austin.framework.test;

/**
 * 手写一个单例模式
 *
 * @author: austin
 * @since: 2023/2/23 14:49
 */
public class Singleton {

    /**
     * volatile修饰的静态实例对象
     */
    private volatile static Singleton INSTANCE;

    /**
     * 构造函数
     */
    public Singleton() {

    }

    public static Singleton getUniqueInstance() {
        // 先判断对象是否已经实例过，没有实例化过才进入加锁代码
        if (INSTANCE == null) {
            // 当前类对象加锁
            synchronized (Singleton.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Singleton();
                }
            }
        }
        return INSTANCE;
    }
}
