package com.austin.framework.test;

/**
 * @author: austin
 * @since: 2023/2/22 12:13
 */
public class InterfaceImpl implements InterfaceA {

    @Override
    public void method() {
        System.out.println("Hi, I am InterfaceImpl, I rewrite the method!");
    }

    @Override
    public void defaultMethod() {
        System.out.println("Hi, I am InterfaceImpl, I rewrite the default method!");
    }

    public static void main(String[] args) {
        InterfaceImpl inter = new InterfaceImpl();
        inter.method();
        inter.defaultMethod();

        // 调用接口的静态方法，只能通过父类直接调用，直接通过类.静态方法调用
        InterfaceA.staticMethod();

        // 调用接口中的常量，直接通过类.属性调用，因为接口中的属性默认为public static final类型修饰
        System.out.println(InterfaceA.COUNT);
    }
}
