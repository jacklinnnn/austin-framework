package com.austin.framework.test;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 对象的深浅拷贝
 *
 * @author: austin
 * @since: 2023/2/22 11:44
 */
public class ObjectCopy {

    @Data
    @AllArgsConstructor
    public static class Address implements Cloneable {

        private String address;

        @Override
        public Address clone() throws CloneNotSupportedException {
            return (Address) super.clone();
        }
    }

    @Data
    @AllArgsConstructor
    public static class User implements Cloneable {

        private Address address;

        ///**
        // * 浅拷贝
        // */
        //@Override
        //public User clone() throws CloneNotSupportedException {
        //    return (User) super.clone();
        //}

        /**
         * 深拷贝
         */
        @Override
        public User clone() throws CloneNotSupportedException {
            User user = (User) super.clone();
            user.setAddress(user.getAddress().clone());
            return user;
        }
    }

    public static void main(String[] args) throws CloneNotSupportedException {
        User user = new User(new Address("广州"));
        User userCopy = user.clone();
        // 深拷贝: true   浅拷贝: false
        System.out.println(user.getAddress() == userCopy.getAddress());
    }
}
