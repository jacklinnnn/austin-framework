package com.austin.framework.test;

/**
 * @author austin
 */
public interface InterfaceA {

    Integer COUNT = 3;

    /**
     * method, 子类可以重写
     */
    void method();

    /**
     * defaultMethod, 子类可以重写
     */
    default void defaultMethod() {
        System.out.println("I am InterfaceA default method");
    }

    /**
     * staticMethod, 静态方法 子类无法重写接口中的static方法
     */
    static void staticMethod() {
        System.out.println("I am InterfaceA static method");
    }

}