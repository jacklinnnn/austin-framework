package com.austin.framework.test;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: austin
 * @since: 2023/2/22 12:10
 */
@Data
@NoArgsConstructor
public class AbstractClassA extends AbstractBaseExample {

    @Override
    public void method() {
        System.out.println("I am AbstractClassA method, i rewrite AbstractBaseExample...");
    }


    @Override
    public String publicPrintMethod() {
        return "I am AbstractClassA printMethod, i rewrite AbstractBaseExample...";
    }

    public static void main(String[] args) {
        AbstractClassA aca = new AbstractClassA();
        aca.COUNT = 666;
        System.out.println(aca.COUNT);

        aca.method();
        aca.publicPrintMethod();

        // 只能通过类的方式调用父类AbstractBaseExample的静态方法
        AbstractClassA.staticMethod();
    }
}
