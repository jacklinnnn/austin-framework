package com.austin.framework.test;

import java.io.File;

/**
 * @author: austin
 * @since: 2023/2/22 15:56
 */
public class PrintDictionaryAndFiles {

    public static void main(String[] args) {
        String path = "C:\\Users\\jacklin";
        File file = new File(path);
        if (!file.exists()) {
            System.out.println(path + " not exists!");
        }
        File[] files = file.listFiles();
        for (int i = 0; i < files.length; i++) {
            File thisFile = files[i];
            if (thisFile.isDirectory()) {
                System.out.println(thisFile.getName() + "，当前file为目录");
            } else {
                System.out.println(thisFile.getName() + "，当前file为文件");
            }
        }
    }
}
