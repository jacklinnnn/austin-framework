package com.austin.framework.test;

/**
 * @author: austin
 * @since: 2023/2/22 12:10
 */
public abstract class AbstractBaseExample {

    Integer COUNT = 3;

    /**
     * method, 抽象类抽象方法
     */
    public abstract void method();

    /**
     * privatePrintMethod，子类无法重写
     */
    private void privatePrintMethod() {
        System.out.println("I am AbstractBaseExample privatePrintMethod!");
    }

    /**
     * publicPrintMethod, 子类可以重写
     */
    public String publicPrintMethod() {
        System.out.println("I am AbstractBaseExample publicPrintMethod!");
        return "I am AbstractBaseExample publicPrintMethod!";
    }

    /**
     * staticMethod, 静态方法 子类无法重写接口中的static方法
     */
    static void staticMethod() {
        System.out.println("I am AbstractBaseExample static method...");
    }
}
