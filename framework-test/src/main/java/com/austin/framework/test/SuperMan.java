package com.austin.framework.test;

import lombok.Data;

/**
 * @author: austin
 * @since: 2023/2/22 11:24
 */
@Data
public class SuperMan extends Man {

    // 父类无法访问子类的hand属性，因为子类的hand属性为private私有的
    //@Override
    //public String write() {
    //    return "use" + hand + "i can't write!";
    //}

    @Override
    public String sing() {
        return "use" + mouth + "i can sing too!";
    }
}
