package com.austin.framework.test;

import java.util.concurrent.Callable;

/**
 * 有三个线程T1、T2、T3, 如何保证顺序执行
 *
 * @author: austin
 * @since: 2023/2/23 20:49
 */
public class JoinTest {


    class NewThread1 implements Runnable {

        @Override
        public void run() {

        }
    }

    class NewThread2 implements Callable {

        @Override
        public Object call() throws Exception {
            return null;
        }
    }

    public static void main(String[] args) {

        final Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("线程1执行...");
            }
        });

        final Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    // 引用线程1，等待t1线程执行完
                    t1.join();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("线程2执行...");
            }
        });

        final Thread t3 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    // 引用线程2，等待t2线程执行完
                    t2.join();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("线程3执行...");
            }
        });

        t3.start();
        t1.start();
        t2.start();
    }
}
