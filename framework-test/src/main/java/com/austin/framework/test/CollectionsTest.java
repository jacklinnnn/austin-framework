package com.austin.framework.test;

import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * @author: austin
 * @since: 2023/3/6 12:05
 */
public class CollectionsTest {

    public static void main(String[] args) {

        List<String> list1 = Arrays.asList("N0001", "N0002", "N0003", "N0005", "N0007");
        List<String> list2 = Arrays.asList("N0001", "N0004", "N0003", "N0006", "N0008");

        // 获取list1有，list2没有的集合（差集）
        Collection subtractList1 = CollectionUtils.subtract(list1, list2);
        System.out.println(subtractList1);

        // 获取list2有，list1没有的集合（差集）
        Collection subtractList2 = CollectionUtils.subtract(list2, list1);
        System.out.println(subtractList2);

        // 交集
        Collection intersection = CollectionUtils.intersection(list1, list2);
        System.out.println(intersection);

        // 并集
        Collection union = CollectionUtils.union(list1, list2);
        System.out.println(union);

        List<List<String>> partition = Lists.partition(list1, 2);
        System.out.println(partition);
    }
}
