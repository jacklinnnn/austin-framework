package com.austin.framework.test;

import lombok.Data;

/**
 * @author: austin
 * @since: 2023/2/22 11:22
 */
@Data
public class Man {

    private String hand;

    public String mouth;


    private String write() {
        return "use " + hand + "i can write!";
    }

    public String sing() {
        return "use " + mouth + "i can sing!";
    }
}
