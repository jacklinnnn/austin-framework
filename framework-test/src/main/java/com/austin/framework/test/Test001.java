package com.austin.framework.test;

import lombok.SneakyThrows;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author: austin
 * @since: 2023/3/5 14:53
 */
public class Test001 {

    @SneakyThrows
    public static Integer statisticsWordCount(String filePath, String regex) {
        BufferedReader reader = null;
        int count = 0;

        try {
            // Read the file
            reader = new BufferedReader(new FileReader(filePath));
            StringBuffer sb = new StringBuffer();
            String str = null;
            while ((str = reader.readLine()) != null) {
                sb.append(str);
            }
            // java.util.regex正则表达式
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(sb);
            while (matcher.find()) {
                count++;
            }

            System.out.println("【" + regex + "】" + "字符串出现次数为：" + count);

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } finally {
            reader.close();
        }

        return count;
    }

    public static void main(String[] args) {
        String path = "E:\\test.txt";
        statisticsWordCount(path, "java");
    }
}
