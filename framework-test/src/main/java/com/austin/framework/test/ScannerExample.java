package com.austin.framework.test;

import java.util.Scanner;

/**
 * @author: austin
 * @since: 2023/2/22 16:13
 */
public class ScannerExample {

    public static void main(String[] args) {
        System.out.println("请输入数字：");
        Scanner scan = new Scanner(System.in);

        double sum = 0;
        double avg = 0;
        int count = 0;

        while (scan.hasNextDouble()) {
            double x = scan.nextDouble();
            // 总数累积
            sum = sum + x;
            // 输入个数累积
            count = count + 1;
        }

        avg = sum / count;
        System.out.println("一共输入了" + count + "个数，中总和为：" + sum);
        System.out.println(count + "个数的平均值为：" + avg);
        scan.close();
    }
}
