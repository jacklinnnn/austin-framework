package com.austin.framework.common.constant;

/**
 * @author: austin
 * @since: 2023/2/21 22:11
 */
public interface FrameworkConstant {

    public static final String FORMAT_DATETIME_WITH_Y_M_D_H_M_S = "yyyy-MM-dd HH:mm:ss";
    /**
     * 验证码原始字符
     */
    public static final String BASE_CHECK_CODES = "qwertyuiplkjhgfdsazxcvbnmQWERTYUPLKJHGFDSAZXCVBNM1234567890";

    /**
     * {@code 500 Server Error} (HTTP/1.0 - RFC 1945)
     */
    public static final Integer INTERNAL_SERVER_ERROR_500 = 500;
    /**
     * {@code 200 OK} (HTTP/1.0 - RFC 1945)
     */
    public static final Integer OK_200 = 200;

    /**
     * 访问权限认证未通过 510
     */
    public static final Integer NO_AUTH = 510;

    /**
     * 没找到资源 404
     */
    public static final Integer NOT_FOUND = 404;

    /**
     * 登录用户Token令牌缓存KEY前缀
     */
    public static final String PREFIX_USER_TOKEN = "prefix_user_token_";
    /**
     * Token缓存时间：3600秒即一小时
     */
    public static final int TOKEN_EXPIRE_TIME = 10 * 3600;
    /**
     * 状态(0无效1有效)
     */
    public static final String STATUS_0 = "0";
    public static final String STATUS_1 = "1";

    public final static String X_ACCESS_TOKEN = "X-Access-Token";

    /**
     * 多租户 请求头
     */
    public final static String TENANT_ID = "tenant_id";

    /**
     * 请求头 用户id
     */
    public final static String USER_ID = "user_id";

    /**
     * 请求头用户名
     */
    public final static String USERNAME = "username";

    /**
     * 验证码缓存到期时间
     */
    public static final Long VERIFY_CODE_EXPIRED = 180L;

    /**
     * 用户账号已锁定
     */
    public static final String USER_ACCOUNT_STATUS_LOCKED = "1";

    /**
     * 用户账号正常
     */
    public static final String USER_ACCOUNT_STATUS_NORMAL = "0";

    /**
     * 租户状态正常
     */
    public static final String TENANT_STATUS_NORMAL = "1";
    /**
     * 租户状态禁用
     */
    public static final String TENANT_STATUS_DISABLE = "0";

}
