package com.austin.framework.common.exception;

import java.io.Serializable;

/**
 * 自定义异常
 *
 * @author: austin
 * @since: 2023/3/15 15:34
 */
public class FrameworkException extends RuntimeException implements Serializable {

    private static final long serialVersionUID = -482621417262958591L;

    public FrameworkException(String message) {
        super(message);
    }

    public FrameworkException(Throwable cause) {
        super(cause);
    }

    public FrameworkException(String message, Throwable cause) {
        super(message, cause);
    }
}
