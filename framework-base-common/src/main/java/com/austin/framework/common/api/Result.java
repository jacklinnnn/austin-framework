package com.austin.framework.common.api;

import com.austin.framework.common.constant.FrameworkConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 接口响应数据对象
 *
 * @author: austin
 * @since: 2023/2/21 22:11
 */
@Data
@ApiModel(value = "接口返回对象", description = "接口返回对象")
@NoArgsConstructor
public class Result<T> implements Serializable {

    private static final long serialVersionUID = 7587785243826274376L;

    /**
     * 成功标志
     */
    @ApiModelProperty(value = "成功标志")
    private boolean success = true;

    /**
     * 返回处理消息
     */
    @ApiModelProperty(value = "返回处理消息")
    private String message = "操作成功！";

    /**
     * 返回代码
     */
    @ApiModelProperty(value = "返回代码")
    private Integer code = 200;

    /**
     * 返回数据对象 data
     */
    @ApiModelProperty(value = "返回数据对象")
    private T result;

    /**
     * 时间戳
     */
    @ApiModelProperty(value = "时间戳")
    private long timestamp = System.currentTimeMillis();

    public Result<T> success(String message) {
        this.message = message;
        this.code = FrameworkConstant.OK_200;
        this.success = true;
        return this;
    }

    public static <T> Result<T> OK() {
        Result<T> r = new Result<T>();
        r.setSuccess(true);
        r.setCode(FrameworkConstant.OK_200);
        r.setMessage("成功");
        return r;
    }

    public static <T> Result<T> OK(T data) {
        Result<T> r = new Result<T>();
        r.setSuccess(true);
        r.setCode(FrameworkConstant.OK_200);
        r.setResult(data);
        return r;
    }

    public static <T> Result<T> OK(String msg, T data) {
        Result<T> r = new Result<T>();
        r.setSuccess(true);
        r.setCode(FrameworkConstant.OK_200);
        r.setMessage(msg);
        r.setResult(data);
        return r;
    }

    public static Result<Object> error(int code, String msg) {
        Result<Object> r = new Result<Object>();
        r.setCode(code);
        r.setMessage(msg);
        r.setSuccess(false);
        return r;
    }

    public Result<T> error500(String message) {
        this.message = message;
        this.code = FrameworkConstant.INTERNAL_SERVER_ERROR_500;
        this.success = false;
        return this;
    }

    public static Result<Object> error(String msg) {
        return error(FrameworkConstant.INTERNAL_SERVER_ERROR_500, msg);
    }

    /**
     * 无权限访问返回结果
     */
    public static Result<Object> noauth(String msg) {
        return error(FrameworkConstant.NO_AUTH, msg);
    }

    /**
     * 404没找到
     */
    public static Result<Object> notFound(String message) {
        return error(FrameworkConstant.NOT_FOUND, message);
    }
}
