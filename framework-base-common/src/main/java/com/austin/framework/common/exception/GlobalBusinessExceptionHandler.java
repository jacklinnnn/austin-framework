package com.austin.framework.common.exception;

import com.austin.framework.common.api.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理
 *
 * @author: austin
 * @since: 2023/3/15 15:36
 */
@Slf4j
@RestControllerAdvice
public class GlobalBusinessExceptionHandler {

    /**
     * 处理自定义异常
     */
    @ExceptionHandler(FrameworkException.class)
    public Result<?> handleFrameworkException(FrameworkException e) {
        log.error(e.getMessage(), e);
        return Result.error(e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public Result<?> handleException(Exception e) {
        log.error(e.getMessage(), e);
        return Result.error("操作失败，" + e.getMessage());
    }
}
