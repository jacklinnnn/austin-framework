package com.austin.framework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ausitn
 */
@SpringBootApplication
public class FrameworkDistributeLockWithRedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(FrameworkDistributeLockWithRedisApplication.class, args);
    }

}
