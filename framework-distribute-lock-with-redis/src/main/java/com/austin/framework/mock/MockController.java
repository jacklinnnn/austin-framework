package com.austin.framework.mock;

import cn.hutool.core.thread.ThreadUtil;
import com.austin.framework.common.api.Result;
import com.baomidou.lock.annotation.Lock4j;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: austin
 * @since: 2023/3/13 11:23
 */
@Slf4j
@RestController
@RequestMapping("/mock")
public class MockController {

    @GetMapping("/lockMethod")
    @Lock4j(name = "#key", acquireTimeout = 1000, expire = 10000)
    public Result lockMethod(String key) {
        log.info("Request invoke... key: {}", key);
        ThreadUtil.sleep(5000);
        return Result.OK(key);
    }
}
