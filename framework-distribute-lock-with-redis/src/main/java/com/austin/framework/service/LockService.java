package com.austin.framework.service;

/**
 * @author: austin
 * @since: 2023/3/15 15:56
 */
public interface LockService {

    /**
     * 手动加锁
     * @param resourceKey
     */
    void lock(String resourceKey);
}
