package com.austin.framework.easyes.mapper;

import cn.easyes.core.conditions.interfaces.BaseEsMapper;
import com.austin.framework.easyes.model.Document;

/**
 * DocumentMapper
 *
 * @author: austin
 * @since: 2023/3/6 14:57
 */
public interface DocumentMapper extends BaseEsMapper<Document> {

}
