package com.austin.framework.easyes.controller;

import cn.easyes.core.conditions.LambdaEsQueryWrapper;
import com.austin.framework.easyes.mapper.DocumentMapper;
import com.austin.framework.easyes.model.Document;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * EsDocumentController
 *
 * @author: austin
 * @since: 2023/3/6 15:00
 */
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class EsDocumentController {

    private final DocumentMapper documentMapper;

    @GetMapping("/insert")
    public Integer insert() {
        // 初始化-> 新增数据
        Document document = new Document();
        document.setId("1");
        document.setTitle("austin");
        document.setContent("austin-framework-elasticsearch");
        return documentMapper.insert(document);
    }

    @RequestMapping("/update")
    public Integer update() {
        // 更新
        Document document = new Document();
        document.setId("1");
        document.setContent("updated content!");
        return documentMapper.updateById(document);
    }

    @GetMapping("/search")
    public List<Document> search() {
        // 查询出所有标题为austin的文档列表
        LambdaEsQueryWrapper<Document> wrapper = new LambdaEsQueryWrapper<>();
        wrapper.eq(Document::getTitle, "austin");
        return documentMapper.selectList(wrapper);
    }

    @DeleteMapping("/delete")
    public Integer delete() {
        // 根据ID删除
        LambdaEsQueryWrapper<Document> esQueryWrapper = new LambdaEsQueryWrapper<>();
        esQueryWrapper.eq(Document::getId, "1");
        return documentMapper.delete(esQueryWrapper);
    }
}
