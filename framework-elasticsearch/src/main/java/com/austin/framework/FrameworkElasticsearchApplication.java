package com.austin.framework;

import cn.easyes.starter.register.EsMapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;


/**
 * @author austin
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class })
@EsMapperScan("com.austin.framework.easyes.mapper")
public class FrameworkElasticsearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(FrameworkElasticsearchApplication.class, args);
    }

}
