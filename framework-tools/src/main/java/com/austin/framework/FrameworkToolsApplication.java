package com.austin.framework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FrameworkToolsApplication {

    public static void main(String[] args) {
        SpringApplication.run(FrameworkToolsApplication.class, args);
    }

}
