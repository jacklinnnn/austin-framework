package com.austin.framework.modules.objectCopy.spring;

/**
 * custom converter
 *
 * @author: austin
 * @date: 2022/11/7 22:17
 */
public interface Converter<E, V> {

    V convert(E e);
}
