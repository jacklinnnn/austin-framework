package com.austin.framework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FrameworkRadissonApplication {

    public static void main(String[] args) {
        SpringApplication.run(FrameworkRadissonApplication.class, args);
    }

}
