package com.austin.framework.cacheableExtend.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author: austin
 * @since: 2023/2/18 18:24
 */
@Slf4j
@Aspect
@Order(2)
@Component
public class CustomCacheableAspect {
}
