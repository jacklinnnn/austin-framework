package com.austin.framework.cacheableExtend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.MethodInvoker;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 缓存注解对象，标记了缓存注解的方法类信息，用于主动刷新缓存时调用原始方法加载数据
 *
 * @author: austin
 * @since: 2023/2/18 18:45
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CacheInvocation {

    private CacheMetaData metaData;

    private Object targetBean;

    private Method targetMethod;

    private Object[] arguments;


    /**
     * 根据相关信息，调用缓存的方法
     *
     * @throws ClassNotFoundException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    public Object invoke() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        final MethodInvoker invoker = new MethodInvoker();
        invoker.setTargetObject(this.getTargetBean());
        invoker.setArguments(this.getArguments());
        invoker.setTargetMethod(this.getTargetMethod().getName());
        invoker.prepare();
        return invoker.invoke();
    }

}
