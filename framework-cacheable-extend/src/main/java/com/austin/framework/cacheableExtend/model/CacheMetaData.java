package com.austin.framework.cacheableExtend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 缓存元数据
 *
 * @author: austin
 * @since: 2023/2/18 18:44
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CacheMetaData {

    private Object key;
    private String[] cacheNames;
    private long expiredTimeSecond;
    private long preloadTimeSecond;
}
