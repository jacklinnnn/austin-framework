package com.austin.framework.cacheableExtend.annotation;

import com.austin.framework.cacheableExtend.constant.CacheConstant;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义注解@CustomCacheable
 *
 * @author: austin
 * @since: 2023/2/18 18:12
 */

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@Cacheable(cacheManager = CacheConstant.CUSTOM_CACHE_MANAGER, keyGenerator = CacheConstant.CUSTOM_CACHE_KEY_GENERATOR)
public @interface CustomCacheable {

    @AliasFor(annotation = Cacheable.class, attribute = "value")
    String[] value() default {};

    @AliasFor(annotation = Cacheable.class, attribute = "cacheNames")
    String[] cacheNames() default {};

    @AliasFor(annotation = Cacheable.class, attribute = "key")
    String key() default "";

    @AliasFor(annotation = Cacheable.class, attribute = "keyGenerator")
    String keyGenerator() default "";

    @AliasFor(annotation = Cacheable.class, attribute = "cacheResolver")
    String cacheResolver() default "";

    @AliasFor(annotation = Cacheable.class, attribute = "condition")
    String condition() default "";

    @AliasFor(annotation = Cacheable.class, attribute = "unless")
    String unless() default "";

    @AliasFor(annotation = Cacheable.class, attribute = "sync")
    boolean sync() default false;

    /**
     * 缓存过期时间
     */
    long expiredTimeSecond() default 0;

    /**
     * 缓存主动在失效前强制刷新缓存的时候，单位：秒
     */
    long preloadTimeSecond() default 0;
}
