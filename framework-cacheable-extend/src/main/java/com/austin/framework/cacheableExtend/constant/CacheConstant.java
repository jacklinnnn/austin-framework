package com.austin.framework.cacheableExtend.constant;

/**
 * @author: austin
 * @since: 2023/2/18 18:19
 */
public interface CacheConstant {

    public static final String CUSTOM_CACHE_MANAGER = "customRedisCacheManager";

    public static final String CUSTOM_CACHE_KEY_GENERATOR = "customKeyGenerator";
}
