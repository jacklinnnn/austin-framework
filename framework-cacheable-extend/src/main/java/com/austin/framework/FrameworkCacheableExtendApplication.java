package com.austin.framework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author asutin
 */
@SpringBootApplication
public class FrameworkCacheableExtendApplication {

    public static void main(String[] args) {
        SpringApplication.run(FrameworkCacheableExtendApplication.class, args);
    }
}
