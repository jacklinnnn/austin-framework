## 🌈项目简介

`ausitn-framework` 整合了目前JAVA主流的技术栈，对不同的技术栈做了相关的实现，研究不同的组件的原理和底层的实现逻辑。

## 📜目录结构

```
Austin-Framework
│
├─framework-base-common                     #公共模块
├─framework-cacheable-extend                #缓存扩展模块    
├─framework-cloud                           #Spring Cloud微服务
├─framework-cloud-alibaba                   #Spring Cloud Alibaba微服务解决方案
├─framework-distribute-lock-with-redis      #Redis分布式锁实现 
├─framework-distributed-transaction         #分布式事务
├─framework-dubbo                           #Dubbo服务治理
├─framework-elasticsearch                   #搜索引擎
├─framework-kafka                           #Kafka
├─framework-mp-generator                    #代码生成器模块
├─framework-rabbitmq                        #RabbitMQ消息中间件 
├─framework-radisson                        #Radisson分布式锁                
├─framework-rocketmq                        #Rocketmq消息中间件
├─framework-sharding-jdbc                   #ShardingJDBC分库分表
├─framework-test                            #测试模块
└─framework-tools                           #工具集 
```
