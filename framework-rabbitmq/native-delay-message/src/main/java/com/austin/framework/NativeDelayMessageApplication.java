package com.austin.framework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NativeDelayMessageApplication {

    public static void main(String[] args) {
        SpringApplication.run(NativeDelayMessageApplication.class, args);
    }

}
