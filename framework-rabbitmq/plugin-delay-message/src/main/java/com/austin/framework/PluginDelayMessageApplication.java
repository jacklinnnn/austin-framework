package com.austin.framework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PluginDelayMessageApplication {

    public static void main(String[] args) {
        SpringApplication.run(PluginDelayMessageApplication.class, args);
    }

}
