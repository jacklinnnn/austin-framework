package com.austin.framework.consumer;

import com.austin.framework.service.HelloService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.PropertySource;

import javax.annotation.PostConstruct;
import java.io.IOException;

/**
 * @author: austin
 * @since: 2023/3/2 23:19
 */
@EnableDubbo
@PropertySource(value = "classpath:/consumer-config.properties")
public class HelloServiceConsumer {


    @DubboReference(version = "${hello.service.version}")
    private HelloService helloService;

    @PostConstruct
    public void init(){
        for (int i = 0; i < 10; i++) {
            System.out.println(helloService.sayHi("Nacos"));
        }
    }

    public static void main(String[] args) throws IOException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(HelloServiceConsumer.class);
        context.refresh();
        context.close();
    }
}
