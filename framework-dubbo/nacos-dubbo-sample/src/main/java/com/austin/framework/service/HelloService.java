package com.austin.framework.service;

/**
 * @author: austin
 * @since: 2023/3/2 23:18
 */
public interface HelloService {

    String sayHi(String name);
}
