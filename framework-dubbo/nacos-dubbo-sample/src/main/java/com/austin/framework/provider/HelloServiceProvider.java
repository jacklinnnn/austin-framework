package com.austin.framework.provider;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.PropertySource;

import java.io.IOException;

/**
 * @author: austin
 * @since: 2023/3/2 23:19
 */
@EnableDubbo(scanBasePackages = "com.austin.framework.service")
@PropertySource(value = "classpath:/provider-config.properties")
public class HelloServiceProvider {

    public static void main(String[] args) throws IOException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(HelloServiceProvider.class);
        context.refresh();
        System.out.println("HelloService provider is starting...");
        System.in.read();
    }
}
