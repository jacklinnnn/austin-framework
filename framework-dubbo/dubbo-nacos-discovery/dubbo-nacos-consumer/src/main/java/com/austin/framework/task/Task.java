package com.austin.framework.task;

import com.austin.framework.api.HelloService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @author: austin
 * @since: 2023/3/1 13:48
 */
@Component
public class Task implements CommandLineRunner {

    /**
     * 通过DubboReference从Dubbo获取一个RPC订阅，这个demoService就可以像本地调用一样直接调用
     */
    @DubboReference(version = "1.0.0")
    private HelloService helloService;


    @Override
    public void run(String... args) throws Exception {

    }
    //
    //@Override
    //public void run(String... args) throws Exception {
    //    String result = helloService.sayHello("world");
    //    System.out.println("------------> Consumer receive result: " + result);
    //
    //    new Thread(() -> {
    //        while (true) {
    //            try {
    //                Thread.sleep(2000);
    //                System.out.println(new Date() + "------------> Consumer receive result: " + helloService.sayHello("world"));
    //            } catch (InterruptedException e) {
    //                throw new RuntimeException(e);
    //            }
    //        }
    //    }).start();
    //}
}
