package com.austin.framework;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 服务消费者
 *
 * @author austin
 */
@EnableDubbo
@SpringBootApplication
@EnableDiscoveryClient
public class DubboNacosConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DubboNacosConsumerApplication.class, args);
    }

}
