package com.austin.framework;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 服务提供者
 *
 * @author ausitn
 */
@EnableDubbo
@SpringBootApplication
@EnableDiscoveryClient
public class DubboNacosProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(DubboNacosProviderApplication.class, args);
    }

}
