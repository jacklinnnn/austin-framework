package com.austin.framework.api;

/**
 * @author: austin
 * @since: 2023/3/1 13:33
 */
public interface HelloService {

    String sayHello(String name);
}
