package com.ausitn.framework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DubboNacosApolloApplication {

    public static void main(String[] args) {
        SpringApplication.run(DubboNacosApolloApplication.class, args);
    }

}
